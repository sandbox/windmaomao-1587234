<?php
/**
 * Implementation of HOOK_theme().
 */
function readymade_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:
  /*
  $hooks['hook_name_here'] = array( // Details go here );
  */
  // @TODO: Needs detailed comments. Patches welcome!
  return $hooks;
}

/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

// function phptemplate_preprocess_page(&$vars) {
// }

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function readymade_preprocess_page(&$vars, $hook) {
  // kpr($vars);
  unset($vars['css']['all']['theme']['sites/all/modules/daemons/daemons.css']);
  unset($vars['css']['all']['module']['modules/user/user.css']);
  // unset($vars['css']['all']['module']['sites/all/modules/nice_menus/nice_menus.css']);
  // unset($vars['css']['all']['module']['sites/all/modules/nice_menus/nice_menus_default.css']);
  $vars['styles'] = drupal_get_css($vars['css']); 
  // kpr($vars);

  // To remove a class from $classes_array, use array_diff().
  //$vars['classes_array'] = array_diff($vars['classes_array'], array('class-to-remove'));

  // kpr($vars['primary_links']);
  // kpr($vars['secondary_links']);

  // update primary and secondary menu
  foreach($vars['primary_links'] as &$link) {
    $link['class'] = 'nav'; 
    $link['attributes']['class'] = 'item';
  }
  foreach($vars['secondary_links'] as &$link) {
    $link['class'] = 'nav'; 
    $link['attributes']['class'] = 'item';
  }
  
  // $user_section = l(
  //   '<span>User Account</span>','',
  //   array(
  //     'attributes' => array(
  //       'class' => 'item icon cog',
  //     ),
  //     'html' => true
  //   )
  // );
  // $user_section .= theme(
  //   'item_list',
  //   array(
  //     l('User','user')
  //   ),
  //   'Account','ul',
  //   array(
  //     'class' => 'menu'
  //   )
  // );
  // kpr($user_section);
  
  $home_section = array(
    'home' => array(
      'class' => 'nav',
      'title' => l(
        '<span>Home</span>','',
        array(
          'attributes' => array(
            'class' => 'item icon',
          ),
          'html' => true
        )
      ),
      'html' => true
    ),
    'bar0' => array(
      'class' => 'bar'
    ),
    'profile' => array(
      'class' => 'nav right',
      'href' => 'user',
      'title' => '<span>User</span>',
      'attributes' => array(
        'class' => 'item icon cog'
      ),
      'html' => true
    )
  );
  
  // $second_section = array(
  //   'bar1' => array(
  //     'class' => 'bar'
  //   )
  // );
  $vars['primary_links'] = $home_section + $vars['primary_links'];
  // kpr($vars['primary_links']);
  // update user menu on the right corner
  // $vars['navigation'] = ;
  
  

  // update mission
  $active_trail = menu_get_active_trail();
//  kpr($active_trail);
  $end = end($active_trail);
  if (isset($end['description']))
    $vars['help'] = $end['description'];
  // kpr($active_trail);
//  foreach($vars['primary_links'] as &$link) {
//    if ((isset($link['href'])) && ($link['href'] == $end['href'])) {
//      $vars['mission'] = isset($link['attributes']['title'])?$link['attributes']['title']:'' ;
//      break;
//    }
//  }
}

function readymade_preprocess_block(&$vars, $hook) {
  $vars['classes_array'][] = 'box';
  // kpr($vars);
}

function readymade_menu_tree($tree) {
  return '<ul>' . $tree . '</ul>';
}

function readymade_links($links, $attributes = array('class' => 'links'), $heading = '') {
  global $language;
  $output = '';

  if (count($links) > 0) {
    // kpr($links);
    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading.
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $output .= '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key. (isset($link['class']) ? (' '.$link['class']) : '');

      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        // $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
        $output .= $link['title'];
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

function readymade_button($element) {
  if (arg(0) == 'admin') return theme_button($element);

  if (isset($element['#attributes']['class'])) {
    $element['#attributes']['class'] .= ' btn small'; 
  } else {
    $element['#attributes']['class'] = 'btn small'; 
  }
  return theme_button($element);
}

// function readymade_nice_menus_build($menu, $depth = -1, $trail = NULL) {
//   // kpr($menu);
//   $output = '';
//   // Prepare to count the links so we can mark first, last, odd and even.
//   $index = 0;
//   $count = 0;
//   foreach ($menu as $menu_count) {
//     if ($menu_count['link']['hidden'] == 0) {
//       $count++;
//     }
//   }
//   // Get to building the menu.
//   foreach ($menu as $menu_item) {
//     $mlid = $menu_item['link']['mlid'];
//     // Check to see if it is a visible menu item.
//     if (!isset($menu_item['link']['hidden']) || $menu_item['link']['hidden'] == 0) {
//       // Check our count and build first, last, odd/even classes.
//       $index++;
//       $first_class = $index == 1 ? ' first ' : '';
//       $oddeven_class = $index % 2 == 0 ? ' even ' : ' odd ';
//       $last_class = $index == $count ? ' last ' : '';
//       // Build class name based on menu path
//       // e.g. to give each menu item individual style.
//       // Strip funny symbols.
//       $clean_path = str_replace(array('http://', 'www', '<', '>', '&', '=', '?', ':', '.'), '', $menu_item['link']['href']);
//       // Convert slashes to dashes.
//       $clean_path = str_replace('/', '-', $clean_path);
//       $class = 'nav menu-path-'. $clean_path;
//       if ($trail && in_array($mlid, $trail)) {
//         $class .= ' active-trail';
//       }
//       $menu_item['link']['localized_options']['attributes']['class'] = 'item';
//       // If it has children build a nice little tree under it.
//       if ((!empty($menu_item['link']['has_children'])) && (!empty($menu_item['below'])) && $depth != 0) {
//         $class .= ' dropdown';
//         // Keep passing children into the function 'til we get them all.
//         $children = theme('nice_menus_build', $menu_item['below'], $depth, $trail);
//         // Set the class to parent only of children are displayed.
//         $parent_class = ($children && ($menu_item['link']['depth'] <= $depth || $depth == -1)) ? 'menuparent ' : '';
//         $output .= '<li class="menu-' . $mlid . ' ' . $parent_class . $class . $first_class . $oddeven_class . $last_class .'">'. theme('menu_item_link', $menu_item['link']);
//         // Check our depth parameters.
//         if ($menu_item['link']['depth'] <= $depth || $depth == -1) {
//           // Build the child UL only if children are displayed for the user.
//           if ($children) {
//             $output .= '<ul>';
//             $output .= $children;
//             $output .= "</ul>\n";
//           }
//         }
//         $output .= "</li>\n";
//       }
//       else {
//         $output .= '<li class="menu-' . $mlid . ' ' . $class . $first_class . $oddeven_class . $last_class .'">'. theme('menu_item_link', $menu_item['link']) .'</li>'."\n";
//       }
//     }
//   }
//   return $output;
// }
/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_node(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // STARTERKIT_preprocess_node_page() or STARTERKIT_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $vars['node']->type;
  if (function_exists($function)) {
    $function($vars, $hook);
  }
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */
