<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"> 
 
<head> 
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
	
</head> 
 
<body> 

<div id="wrapper" class="">
	
	<div id="header">		
		<div class="container">
			<div class="grid grid_24">
			  
        <?php if ($primary_links || $navigation): ?>
          <?php 
            print theme(
              array('links__system_main_menu', 'links'), $primary_links,
              array('id' => 'main-menu','class' => 'menu-list clearfix')
            );
          ?>
          <?php print $navigation; ?>
			  <?php endif; ?>
				
			</div> <!-- .grid -->
		</div> <!-- .container -->
	</div> <!-- #header -->
	
	<div id="sub-header">		
		<div class="container">
			
			<div class="grid grid_24">
          <?php 
            print theme(
              array('links__system_main_menu', 'links'), $secondary_links,
              array('id' => 'sub-menu','class' => 'menu-list clearfix fr')
            );
          ?>
    			<?php if (!empty($title)): ?><h2 class="title" id="page-title"><?php print $title; ?></h2><?php endif; ?>
    			  
			</div> <!-- .grid -->			
		</div> <!-- .container -->
	</div> <!-- #sub-header -->
	

	<div id="content">				
		<div class="container">

      <?php if (!empty($sidebar_first)): ?> 
        <div class="grid grid_6">
          <?php print $sidebar_first; ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($sidebar_first)): ?> 
			<div class="grid grid_18">
      <?php else: ?>
			<div class="grid grid_24">
      <?php endif; ?>
          <?php print $highlight; ?>
          <?php if (!empty($messages)): print $messages; endif; ?>
          <?php print $breadcrumb; ?>
          <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
          <?php if (!empty($help)): print $help; endif; ?>
            <?php print $content; ?>
			</div> <!-- .grid -->
		
		</div> <!-- .container -->
	</div> <!-- #content -->

	<!-- fix IE7 issue -->
  <!--[if IE]>
  <div style="height:300px;">&nbsp;</div>
  <![endif]-->

	<div id="footer" class="cf">		
		<div class="container">
			<div class="grid grid_8">
        <?php if (!empty($footer_first)): ?> 
          <?php print $footer_first; ?>
        <?php endif; ?>
			</div> <!-- .grid -->
			
			<div class="grid grid_8 prepend_1">
        <?php if (!empty($footer_second)): ?> 
          <?php print $footer_second; ?>
        <?php endif; ?>
			</div> <!-- .grid -->			
			
			<div class="grid grid_6 prepend_1">
        <?php if (!empty($footer_third)): ?> 
          <?php print $footer_third; ?>
        <?php endif; ?>
			</div> <!-- .grid -->
			
			<div id="copyright" class="grid grid_24">
				<div class="grid grid_12 left">
				<?php print $footer_message; ?>
				</div> <!-- .grid -->
				
				<div class="grid grid_12 right">
					
					<a href="./dashboard.html">Dashboard</a>
					&nbsp;&nbsp;
					<a href="javascript:;">Terms of Use</a>
					&nbsp;&nbsp;
					<a href="javascript:;">Privary Policy</a>
				</div> <!-- .grid -->			
				
			</div> <!-- .grid -->
			
		</div> <!-- .container -->		
	</div> <!-- #footer -->

  <?php print $closure; ?>

</div> <!-- #wrapper -->

</body> 
 
</html>
